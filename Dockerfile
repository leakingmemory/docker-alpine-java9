FROM alpine:3.7
RUN apk update
RUN apk add ca-certificates wget
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub
RUN wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.27-r0/glibc-2.27-r0.apk
RUN apk add glibc-2.27-r0.apk
COPY 'jdk-9.0.4_linux-x64_bin.tar.gz' /
RUN tar xzvf 'jdk-9.0.4_linux-x64_bin.tar.gz'
RUN ln -s /lib/libz.so.1 /usr/glibc-compat/lib
RUN ln -s /usr/glibc-compat/lib/libc.so.6 /usr/glibc-compat/lib/libc.musl-x86_64.so.1
ENV JAVA_HOME="/jdk-9.0.4"
ENV PATH="$PATH:/jdk-9.0.4/bin"
ENV LD_LIBRARY_PATH="/jdk-9.0.4/lib:/jdk-9.0.4/jli"
